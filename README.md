wp-soap-services
================

Exposing the Wordpress codex through SOAP Web Services

Requirements
============
- Wordpress 3.x
- PHP 5.3 with SOAP extensions activated (check phpinfo to verify SOAP is active)
- LAMP/WAMP/XAMPP/MAMP combined web server/mysql compatible with Wordpress

Publication
===========

The following publication, "Adding Soap Web Services to Wordpress" available on
Amazon at http://www.amazon.com/dp/B00D1AYIG8 shows how to implement this framework plugin
in your wordpress blog.
